// We define classes with the use of the keyword "class" and a set of {}
	// Naming convention dictates that the class names should begin with an uppercase letter and singular entity/object

class Student{
	// contstructor keyword allows us to pass arguments when we instantiate object from classes 
	grades = [];
	constructor(name, email, grades){
		this.passed = undefined;
		this.passedWithHonors = undefined;
		this.gradeAve = undefined;
		this.name = name;
		this.email = email;
		// this.grades = grades;
		if (grades.every(grade => (grade >=0 && grade <=100 && grades.length === 4 && typeof(grade) === "number"))){
			this.grades= grades;
		} else {
			this.grades = undefined
		}
	}

	login(){
		console.log(`${this.email} has logged in`)
		return this;
	}

	logout(){
		console.log(`${this.email} has logged out`)
		return this;
	}

	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
		return this;
	}

	computeAve(){
        let grades = this.grades
        let sum = 0;
	    this.grades.forEach(grade => sum = sum + grade);
        let avg = sum / grades.length
        // return avg
        this.gradeAve = avg
        return this;
        // this.grades.forEach(grade => sum = sum + grade)
    }

    willPass(){
        let ave = this.computeAve()
        if(ave >= 85){
            this.passed = true
        } else {
            this.passed = false
        }
        return this;
    }

    willPassWithHonors(){
        if (this.willPass()){
        	if(this.computeAve() >= 90){
        		this.passedWithHonors = true
        	} else {
        		this.passedWithHonors = false
        	}
        } else {
        	return undefined
        }
        return this;
    }
}

let studentOne = new Student("John", "john@mail.com", [89,84,78,88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78,82,79,85]);
let studentThree = new Student("Jane", "jane@mail.com", [87,89,91,93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91,89,92,93]);

/*
	QUIZ 1

	1. What is the blueprint where objects are created from?
		// Class
	
	2. What is the naming convention applied to classes?
		// should begin with an uppercase letter and singular entity/object

	3. What keyword do we use to create objects from a class?
		// new

	4. What is the technical term for creating an object from a class?
		// instantiation

	5. What class method dictates HOW objects will be created from that class?
		// constructor 
*/

/*

	ACITIVITY 1

	1. Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.

	2. Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.

*/


/*
	QUIZ 2

	1. Should class methods be included in the class constructor?
		// NO

	2. Can class methods be separated by commas?
		// NO

	3. Can we update an object’s properties via dot notation?
		// NO

	4. What do you call the methods used to regulate access to an object’s properties?
		// 

	5. What does a method need to return in order for it to be chainable?
		// return this

*/

/*
	ACTIVITY 2

	Modify the Student class to allow the willPass() and willPassWithHonors() methods to be chainable.

*/











